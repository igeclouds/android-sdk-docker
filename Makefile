all: build push sign sbom
default: build

-include vars.mk
include constants.mk

## BUILDING OPTIONS
ANDROID_API_VERSION ?=
### one of the template variants
export BUILD_VARIANT ?=
### Prefix for the image, like debian, alpine, scratch
export TAG_PREFIX ?= $(BUILD_VARIANT)
TAG_PREFIX_ALIASES ?=

### debian:bullseye, alpine:3.17, node:20-alpine, etc (ignored when building scratch)
BASE_IMAGE ?=
### ignored when building scratch
JDK_VERSION ?= 11
ifdef $(CI_PIPELINE_CREATED_AT)
ISO_DATE = $(CI_PIPELINE_CREATED_AT)
else
ISO_DATE = $(shell date -u +%Y-%m-%dT%H:%M:%SZ)
endif
URLSAFE_DATE :::= $(subst :,-,$(ISO_DATE))

## Could use guile function, but Alpine distribution of GNU Make doesn't integrate guile feature
ifneq ($(filter $(shell echo $(AUTO_PUSH) | tr '[:upper:]' '[:lower:]'),true 1),)
PUSH_ON_BUILD :::= true
else
PUSH_ON_BUILD :::= false
endif

ifneq ($(filter $(shell echo $(INSECURE_REGISTRY) | tr '[:upper:]' '[:lower:]'),true 1),)
BUILD_TLS_OPTS :::= --tls-verify=false
PUSH_TLS_OPTS :::= --tls-verify=false
SIGN_TLS_OPTS :::= --allow-insecure-registry
DIGEST_TLS_OPTS :::= --tls-verify=false
ARTIFACT_TLS_OPTS :::= --plain-http --insecure
else
BUILD_TLS_OPTS :::= --tls-verify
PUSH_TLS_OPTS :::= --tls-verify
SIGN_TLS_OPTS :::=
DIGEST_TLS_OPTS :::= --tls-verify
ARTIFACT_TLS_OPTS :::=
endif

## COMPUTED CONSTANTS
BUILD_API_TARGETS :::= $(addprefix build-api-,$(subst $(comma), ,$(ANDROID_API_VERSIONS)))
PUSH_API_TARGETS :::= $(addprefix push-api-,$(subst $(comma), ,$(ANDROID_API_VERSIONS)))
SIGN_API_TARGETS :::= $(addprefix sign-api-,$(subst $(comma), ,$(ANDROID_API_VERSIONS)))
SBOM_API_TARGETS :::= $(addprefix sbom-$(TAG_PREFIX)-api-,$(subst $(comma), ,$(ANDROID_API_VERSIONS)))
AUDIT_API_TARGETS :::= $(addprefix audit-api-,$(subst $(comma), ,$(ANDROID_API_VERSIONS)))
PLATFORMS :::= $(subst $(comma), , $(BUILD_PLATFORM))

export SUBMAKE = $(MAKE) --no-print-directory

## EXPORT variables from vars.mk
export COSIGN_PASSWORD
export COSIGN_PRIVATE_KEY
export COSIGN_PUBLIC_KEY
export TEMP_DIR

## FUNCTIONS
pretty_platform = $(subst /,,$(patsubst linux/%,%,$(1)))
get_list_digest = $(shell \
	DIGEST_TLS_OPTS="$(DIGEST_TLS_OPTS)" \
	bash utils/get-digest.sh "$(1)" \
)
get_platform_digest = $(shell \
	DIGEST_TLS_OPTS="$(DIGEST_TLS_OPTS)" \
	bash utils/get-digest.sh "$(1)" "$(2)" \
)
get_aliases = $(shell \
	TAG_PREFIX_ALIASES=$(TAG_PREFIX_ALIASES) \
	EXTRA_PREFIX_ALIASES=$(EXTRA_PREFIX_ALIASES) \
	ALIAS_PREFIX_DEFAULT=$(ALIAS_PREFIX_DEFAULT) \
	ALIAS_SUFFIX_DEFAULT=$(ALIAS_SUFFIX_DEFAULT) \
	ALIAS_SUFFIX_LATEST=$(ALIAS_SUFFIX_LATEST) \
	ALIAS_SUFFIX_STABLE=$(ALIAS_SUFFIX_STABLE) \
	ALIAS_SUFFIX_NEXT=$(ALIAS_SUFFIX_NEXT) \
	bash utils/get-aliases.sh "$(TAG_PREFIX)" "$(1)" "$(REPO_NAME)" \
)
get_api_versions = $(subst $(comma), , $(ANDROID_API_VERSIONS))
get_nodejs_versions = $(shell \
	bash utils/expand-versions.sh \
	$(subst v,,$(shell $(BUILDER) run --rm --entrypoint node $(BASE_IMAGE) --version))\
)
get_nodejs_extra_prefix_aliases = $(shell \
	BUILD_VARIANT=$(BUILD_VARIANT) \
	BASE_IMAGE=$(BASE_IMAGE) \
	bash utils/nodejs-extra-aliases.sh $(call get_nodejs_versions) \
)

## TEMP DIR
$(TEMP_DIR):
	@mkdir -p $@

## CLEAN
clean:
	@rm -fr "${TEMP_DIR}"

## ARTIFACTS
.PHONY: artifacts
artifacts: $(TEMP_DIR)/license.json $(TEMP_DIR)/publickey.json $(TEMP_DIR)/readme.json

$(TEMP_DIR)/publickey.json: $(TEMP_DIR)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg output_file cosign.pub \
		--arg input_file ${COSIGN_PUBLIC_KEY} \
		--arg file_description "${PUBLICKEY_DESCRIPTION}" \
		--arg date $(ISO_DATE) > $@
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $@ \
		$(REPO_NAME):publickey,publickey_$(URLSAFE_DATE) \
		${COSIGN_PUBLIC_KEY}:${PEM_TYPE}

$(TEMP_DIR)/license.json: $(TEMP_DIR)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg input_file LICENSE.md \
		--arg output_file LICENSE.md \
		--arg file_description "${LICENSE_DESCRIPTION}" \
		--arg date $(ISO_DATE) > $@
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $@ \
		$(REPO_NAME):license,license_$(URLSAFE_DATE) \
		LICENSE.md:${MARKDOWN_TYPE}

$(TEMP_DIR)/readme.json: $(TEMP_DIR)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg input_file README.md \
		--arg output_file README.md \
		--arg file_description "${README_DESCRIPTION}" \
		--arg date $(ISO_DATE) > $@
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $@ \
		$(REPO_NAME):readme,readme_$(URLSAFE_DATE) \
		README.md:${MARKDOWN_TYPE}

## BUILD
.PHONY: build
build: build-cmdline build-tools $(BUILD_API_TARGETS)

.PHONY: build-cmdline
build-cmdline:
	$(eval TAG_SUFFIX = $(subst build-,,$@))
	@echo Building $(TAG_PREFIX)-$(TAG_SUFFIX)
	$(eval context = ${TEMPLATES_DIR}/$(BUILD_VARIANT)/$(TAG_SUFFIX))
	@${BUILDER} build ${BUILD_OPTS} $(BUILD_TLS_OPTS) \
		--manifest $(REPO_NAME):$(TAG_PREFIX)-$(TAG_SUFFIX) \
		--platform ${BUILD_PLATFORM} \
		--build-arg ASDK_REPO="$(ASDK_REPO)" \
		--build-arg BASE_IMAGE="${BASE_IMAGE}" \
		--build-arg CMDLINE_TOOLS_VERSION="${CMDLINE_TOOLS_VERSION}" \
		--build-arg JDK_MIRROR="${JDK_MIRROR}" \
		--build-arg JDK_VERSION="${JDK_VERSION}" \
		--file $(context)/Containerfile \
		$(context)
	@echo !!!CMDLINE BUILD DONE!!!
	$(if $(filter $(PUSH_ON_BUILD),true), \
		@$(SUBMAKE) push-$(TAG_SUFFIX) \
	)

.PHONY: build-tools
build-tools: build-cmdline
	$(eval base_image_suffix = $(subst build-,,$<))
	$(eval TAG_SUFFIX = $(subst build-,,$@))
	@echo Building $(TAG_PREFIX)-$(TAG_SUFFIX)
	$(eval context = ${TEMPLATES_DIR}/$(BUILD_VARIANT)/$(TAG_SUFFIX))
	@${BUILDER} build ${BUILD_OPTS} $(BUILD_TLS_OPTS) \
		--manifest $(REPO_NAME):$(TAG_PREFIX)-$(TAG_SUFFIX) \
		--platform ${BUILD_PLATFORM} \
		--build-arg ANDROID_BUILD_TOOLS_VERSION="${ANDROID_BUILD_TOOLS_VERSION}" \
		--build-arg ASDK_REPO="$(ASDK_REPO)" \
		--build-arg BASE_IMAGE="$(REPO_NAME):$(TAG_PREFIX)-$(base_image_suffix)" \
		--build-arg JDK_VERSION="${JDK_VERSION}" \
		--file $(context)/Containerfile \
		$(context)
	@echo !!!TOOLS BUILD DONE!!!
	$(if $(filter $(PUSH_ON_BUILD),true), \
		@$(SUBMAKE) push-$(TAG_SUFFIX) \
	)

.PHONY: build-api-%
$(BUILD_API_TARGETS): build-api-%: build-tools
	$(eval base_image_suffix = $(subst build-,,$<))
	$(eval ANDROID_API_VERSION = $(subst build-api-,,$@))
	@echo Building $(TAG_PREFIX)-api-$(ANDROID_API_VERSION)
	$(eval context = ${TEMPLATES_DIR}/$(BUILD_VARIANT)/api-x)
	@${BUILDER} build ${BUILD_OPTS} $(BUILD_TLS_OPTS) \
		--manifest $(REPO_NAME):$(TAG_PREFIX)-api-$(ANDROID_API_VERSION) \
		--platform ${BUILD_PLATFORM} \
		--build-arg ANDROID_API_VERSION="$(ANDROID_API_VERSION)" \
		--build-arg ASDK_REPO="$(ASDK_REPO)" \
		--build-arg BASE_IMAGE="$(REPO_NAME):$(TAG_PREFIX)-$(base_image_suffix)" \
		--build-arg JDK_VERSION="${JDK_VERSION}" \
		--file $(context)/Containerfile \
		$(context)
	@echo !!!API-$(ANDROID_API_VERSION) BUILD DONE!!!
	$(if $(filter $(PUSH_ON_BUILD),true), \
		@$(SUBMAKE) push-api-$(ANDROID_API_VERSION) \
	)

## PUSH
.PHONY: push
push: push-cmdline push-tools $(PUSH_API_TARGETS)

.PHONY: push-%
push-cmdline push-tools $(PUSH_API_TARGETS): push-%:
	$(eval TAG_SUFFIX = $(subst push-,,$@))
	@echo Pushing ${REPO_NAME}:$(TAG_PREFIX)-$(TAG_SUFFIX)
	$(eval manifest_name = ${REPO_NAME}:$(TAG_PREFIX)-$(TAG_SUFFIX))
	@${BUILDER} manifest push --all ${PUSH_OPTS} $(PUSH_TLS_OPTS) $(manifest_name) docker://$(manifest_name)
	@echo Pushed $(manifest_name)
	$(if $(findstring node:, ${BASE_IMAGE}), \
		$(eval EXTRA_PREFIX_ALIASES = $(call get_nodejs_extra_prefix_aliases)) \
	)
	@$(foreach alias, $(call get_aliases,$(TAG_SUFFIX)), \
		$(BUILDER) manifest push --all ${PUSH_OPTS} $(PUSH_TLS_OPTS) $(manifest_name) docker://$(alias) || exit $$? ; \
		echo Pushed $(alias) ; \
	)

## SIGN
.PHONY: sign
sign: sign-cmdline sign-tools $(SIGN_API_TARGETS)

.PHONY: sign-%
sign-cmdline sign-tools $(SIGN_API_TARGETS): sign-%:
	$(eval TAG_SUFFIX = $(subst sign-,,$@))
	@echo Signing $(TAG_PREFIX)-$(TAG_SUFFIX)
	$(eval digest = $(call get_list_digest,$(REPO_NAME):$(TAG_PREFIX)-$(TAG_SUFFIX)))
	@cosign sign $(SIGN_TLS_OPTS) --yes --recursive --key ${COSIGN_PRIVATE_KEY} $(REPO_NAME)@$(digest)
	@cosign verify $(SIGN_TLS_OPTS) --key ${COSIGN_PUBLIC_KEY} $(REPO_NAME)@$(digest) > /dev/null

## SBOM
.PHONY: sbom
sbom: sbom-$(TAG_PREFIX)-cmdline sbom-$(TAG_PREFIX)-tools $(SBOM_API_TARGETS)

.PHONY: sbom-$(TAG_PREFIX)-%
sbom-$(TAG_PREFIX)-cmdline sbom-$(TAG_PREFIX)-tools $(SBOM_API_TARGETS): sbom-$(TAG_PREFIX)-%:
	$(eval TAG_SUFFIX = $(subst sbom-$(TAG_PREFIX)-,,$@))
	@$(foreach platform, $(PLATFORMS), \
		$(eval arch = $(call pretty_platform,$(platform))) \
		arch=$(arch) \
		platform=$(platform) \
		TAG_SUFFIX=$(TAG_SUFFIX) \
		$(SUBMAKE) $(TEMP_DIR)/sbom-$(TAG_PREFIX)-$(TAG_SUFFIX)-$(arch).syft.json || exit $$? ; \
	)

$(TEMP_DIR)/sbom-$(TAG_PREFIX)-$(TAG_SUFFIX)-$(arch).syft.json: $(TEMP_DIR)
	@echo Generating SBOM for $(TAG_PREFIX)-$(TAG_SUFFIX), $(arch)
	$(eval digest = $(call get_platform_digest,${REPO_NAME}:$(TAG_PREFIX)-$(TAG_SUFFIX),$(platform)))
	@syft packages \
		--output syft-json=$@ \
		--output spdx-json=$(subst syft,spdx,$@) \
		$(REPO_NAME)@$(digest)
	@cosign attach sbom \
		--type syft \
		--sbom $@ \
		$(REPO_NAME)@$(digest)
	@cosign sign --yes \
		--attachment sbom \
		--key ${COSIGN_PRIVATE_KEY} \
		$(REPO_NAME)@$(digest)
	@cosign verify \
		--attachment sbom \
		--key ${COSIGN_PUBLIC_KEY} \
		$(REPO_NAME)@$(digest) > /dev/null
	@cosign attest --yes \
		--type spdxjson \
		--predicate $(subst syft,spdx,$@) \
		--key ${COSIGN_PRIVATE_KEY} \
		$(REPO_NAME)@$(digest)
	@cosign verify-attestation \
		--type spdxjson \
		--key ${COSIGN_PUBLIC_KEY} \
		$(REPO_NAME)@$(digest) > /dev/null

.PHONY: audit
audit: audit-cmdline audit-tools $(AUDIT_API_TARGETS)

.PHONY: audit-%
audit-cmdline audit-tools $(AUDIT_API_TARGETS): audit-%:
	$(eval TAG_SUFFIX = $(subst audit-,,$@))
	@$(foreach platform, $(PLATFORMS), \
		$(eval digest = $(call get_platform_digest,$(REPO_NAME):$(TAG_PREFIX)-$(TAG_SUFFIX),$(platform))) \
		$(eval arch = $(call pretty_platform,$(platform))) \
		$(eval tag_arch = $(TAG_PREFIX)-$(TAG_SUFFIX)-$(arch)) \
		digest="$(digest)" $(SUBMAKE) $(TEMP_DIR)/grype-$(tag_arch).txt || exit $$? ; \
		syft convert $(TEMP_DIR)/remote-sbom-$(tag_arch).json -o syft-table=$(TEMP_DIR)/remote-sbom-$(tag_arch).txt ; \
	)

.PRECIOUS: $(TEMP_DIR)/remote-sbom-%.json
$(TEMP_DIR)/remote-sbom-%.json: $(TEMP_DIR)
	$(eval tag_arch = $(subst remote-sbom-,,$(notdir $(basename $@))))
	@echo Verifying and Downloading SBOM for $(tag_arch)
	@cosign verify --attachment sbom --key ${COSIGN_PUBLIC_KEY} ${REPO_NAME}@$(digest) > /dev/null
	@cosign download sbom --output-file $@ ${REPO_NAME}@$(digest)

$(TEMP_DIR)/grype-%.txt: $(TEMP_DIR)/remote-sbom-%.json
	$(eval tag_arch = $(subst grype-,,$(notdir $(basename $@))))
	@echo Checking vulnerabilities in $(tag_arch)
	@grype --file $@ sbom:$<
