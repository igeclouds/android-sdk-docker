# ReDemoNBR's Android SDK

## Summary
-   [What is this image](#what-is-this-image)
-   [BREAKING CHANGES WARNING](#breaking-change-warning)
-   [Tags explanation](#tags-explanation)
    -   [Base Images (aka flavors)](#base-images-aka-flavors)
    -   [Image Variants and API Levels](#image-variants-and-api-levels)
    -   [Distributed Architectures](#distributed-architectures)
-   [Source of these images](#source-of-these-images)
-   [Environment Variables](#environment-variables)
    -   [Automatically Accepting Licenses](#automatically-accepting-licenses)
    -   [Locales Variables](#locales-variables)
-   [Using sdkmanager CLI](#using-sdkmanager-cli)
-   [Continuous Integration example](#continuous-integration-example)
    -   [GitLab-CI](#gitlab-ci)
-   [Note for Apache Cordova users on NodeJS image variants](#note-for-apache-cordova-users-on-nodejs-image-variants)
-   [Note for deprecated images](#note-for-deprecated-images)
-   [License](#license)
-   [FAQ](#faq)


## What is this image
This image contains required tooling for building Android applications.
It contains (at the time of this writing):
-   Base OS tooling (from base Debian, Ubuntu or Alpine images)
-   OpenJDK 11 and Gradle installation
-   Android Command Line Tools (version `9477386`) with:
    -   Build Tools (version `34.0.0`) _(not included in "cmdline" variants)_
    -   Platform Tools (version `34.0.3`) _(not included in "cmdline" variants)_
    -   Platform (version will depend on the [tag](#tags-explanation)). Also known as `API Level` (refer to [API Levels](#api-levels)) _(not included in "cmdline" and "tools" variants)_
    -   Build Tools, Command Line Tools and Platform Tools are also added to `PATH`

## BREAKING CHANGE WARNING
Since 2023-02-12, the built images require to accept the Google's Android SDK licenses.
A prompt with the licences will be displayed on entrypoint unless `ASDK_PROMPT_LICENSES` is set not to (or the entire entrypoint is overwritten).
It can be accepted manually or automatically (in case you already reviewed the license and agree to it).
Without any customization, the default behavior is to prompt for the licenses on container entrypoint so you can review and accept them manually.

But, to automatically accept the licenses on entrypoint, set the environment variable `ASDK_ACCEPT_LICENSES` to `yes`, `true` or `1`.
If `ASDK_ACCEPT_LICENSES` is unset (default behavior) or contain any other value, the container will prompted to accept the licenses.

For more information about these and other Environment Variables, scroll down to [Environment Variables](#environment-variables) section.

## Tags explanation
Format is: `<base_image>-<variant>` or `<base_image>-api-<level>`.

### Base images (aka flavors)
Base image refer to the underlying image used to build this image on top of.
-   `alpine3.18` (alias: `alpine3`, `alpine`): refer to [`alpine`](https://hub.docker.com/_/alpine) image. Latest stable release from Alpine Linux
-   `alpine3.17`: refer to [`alpine`](https://hub.docker.com/_/alpine) image. Latest stable release on security fixes support from Alpine Linux
-   `alpine3.16`: refer to [`alpine`](https://hub.docker.com/_/alpine) image. Previous stable release on security fixes support from Alpine Linux
-   `alpine3.15` (DEPRECATION NOTICE): refer to [`alpine`](https://hub.docker.com/_/alpine) image. Old stable release on security fixes support from Alpine Linux
-   `bookworm` (alias: `debian`): refer to [`buildpack_deps:bookworm`](https://hub.docker.com/_/buildpack_deps) image. Latest release from Debian Linux
-   `bullseye`: refer to [`buildpack_deps:bullseye`](https://hub.docker.com/_/buildpack_deps) image. Previous release from Debian Linux
-   `buster`: refer to [`buildpack_deps:buster`](https://hub.docker.com/_/buildpack_deps) image. Old release from Debian Linux
-   `focal`: refer to [`buildpack_deps:focal`](https://hub.docker.com/_/buildpack_deps) image. Previous LTS release from Ubuntu Linux
-   `jammy` (alias: `ubuntu`): refer to [`buildpack_deps:jammy`](https://hub.docker.com/_/buildpack_deps) image. Lastest LTS release from Ubuntu Linux
-   `lunar`: refer to [`buildpack_deps:lunar`](https://hub.docker.com/_/buildpack_deps) image. Latest interim (rolling) release from Ubuntu Linux
-   `node-20` (alias: `node`): refer to [`node:20`](https://hub.docker.com/_/node) image. Latest stable release from NodeJS, it uses `buildpack_deps:bookworm` as base
-   `node-20-alpine` (alias: `node-alpine`): refer to [`node:20-alpine`](https://hub.docker.com/_/node) image. Latest stable release from NodeJS, it uses `alpine:3.18` as base
-   `node-18` (alias: `node-hydrogen`, `node-lts`): refer to [`node:18`](https://hub.docker.com/_/node) image. Latest LTS from NodeJS, it uses `buildpack_deps:bookworm` as base
-   `node-18-alpine` (alias: `node-hydrogen-alpine`, `node-lts-alpine`): refer to [`node:18-alpine`](https://hub.docker.com/_/node) image. Latest LTS release from NodeJS, it uses `alpine:3.18` as base
-   `node-16` (alias: `node-gallium`) (DEPRECATION NOTICE): refer to [`node:16`](https://hub.docker.com/_/node) image. Previous LTS release from NodeJS, it uses `buildpack_deps:bookworm` as base
-   `node-16-alpine` (alias: `node-gallium-alpine`) (DEPRECATION NOTICE): refer to [`node:16-alpine`](https://hub.docker.com/_/node) image. Previous LTS release from NodeJS, it uses `alpine:3.18` as base
-   `scratch`: refer to [scratch](https://hub.docker.com/_/scratch/). Scratch-based image without an underlying OS. Useful for building your own images with Android SDK by `COPY`-ing `/android-sdk` directory. Only use it if you know what you are doing

### Image Variants and API Levels
#### `redemonbr/android-sdk:<version>` | `redemonbr/android-sdk:<base-image>-<api-level>`
This is the defacto image, it bundles `platform-tools`, `build-tools` and Android Platform installed. The platform version installed depends on the API Level variant.
The API Level is also known as SDK version or simply "platform", this is the settings level that determine the compatibility with Android versions.
-   `api-34` (alias: `next`): The next level API available at the time of this writing that is available under _Beta_ status. Corresponds to [**Android 14**](https://developer.android.com/about/versions/14). _Might be considered unstable_
-   `api-33` (alias: `latest`): The latest API level that mainly focus on privacy and permission control, and better experience for large screen devices. This will be the minimum target API level allowed by Google Play for new apps starting on 2023-08-31. Corresponds to [**Android 13**](https://developer.android.com/about/versions/13).
-   `api-32`: The stable API level that mainly contains features for foldable devices on top of API 31. Corresponds to [**Android 12L**](https://developer.android.com/about/versions/12/12L)
-   `api-31` (alias: `stable`): The stable API level available at the time of this writing. This will be the minimum target API level allowed by Google Play for existing apps starting on 2023-08-31. Corresponds to [**Android 12**](https://developer.android.com/about/versions/12)
-   `api-30`: The minimum target API level allowed by Google Play for Wear OS applications. Corresponds to [**Android 11**](https://developer.android.com/about/versions/11)

For more information on SDK Level requirements in Google Play, please read [this](https://developer.android.com/google/play/requirements/target-sdk) and [this](https://support.google.com/googleplay/android-developer/answer/11926878).

If `redemonbr/android-sdk:<version>` format is used (ex: `docker pull redemonbr/android-sdk:api-32`), the base image version is `jammy`
For more information about the tags, please check the list of available tags in Docker Hub

#### `redemonbr/android-sdk:<base-image>-tools` (tools)
This image is identical to `cmdline` variants, but also bundles `platform-tools` and `build-tools`. This is a good image to be extended in case we are not supporting some of the API Levels here.
So you can create your Dockerfile with `FROM redemonbr/android-sdk:<version>-tools` and extend it.
Examplifying: the defacto images above are an extension of this one. Check our GitLab repositories to see how the Dockerfile extends this one.

#### `redemonbr/android-sdk:<base-image>-cmdline` (cmdline)
This image contains only the Android command line tools (aka `cmdline`) installed with the `sdkmanager` configured to be executed along with OpenJDK 11 and Gradle. It does not contain `platform-tools`, `build-tools` or any of the `platforms` installed for building applications.
This is a good image if you want to extend the base image installing different versions of `build-tools` or `platform`, or simply having a more robust tooling for building extremely complex Android applications.
In this case, create your own Dockerfile with `FROM redemonbr/android-sdk:<version>-cmdline` and extend it.
Examplifying: the `tools` variant above is an extension of this one. Check our GitLab repositories to see how the Dockerfile extends this one.


### Distributed Architectures
The Container Images are available in 2 different architectures: `amd64` and `arm64/v8`.
While most of the usages will be `amd64`, some users with `arm64/v8` host machines (like Apple devices using Apple Silicon CPU) may take the advantage of optimizations for its own platform.

However, **images distributed in `arm64/v8` are in experimental phase** at this moment.
If you encounter any issue with `arm64/v8` and emulation for `amd64` is available, you can run the `amd64` containers like `docker run --platform linux/amd64 redemonbr/android-sdk:ubuntu-api-33`


## Source of these images
-   Repository: <https://gitlab.com/rdnxk/android-sdk-docker>
-   Issues: <https://gitlab.com/rdnxk/android-sdk-docker/-/issues>
-   Dockerfiles: <https://gitlab.com/rdnxk/android-sdk-docker/-/tree/master/templates>

These images are updated on a weekly basis.


## Environment Variables
The images generated contains entrypoint scripts that may help you in customizing the image. For the basic functionality, nothing needs to be done.
But if you need some extra customization, these might be helpful.
By nature, `scratch`-based variants have no environment variables nor entrypoint, thus this section does not apply to them.

-   **`ASDK_PROMPT_LICENSES`:** Whether or not to prompt to accept licenses on entrypoint. Will prompt if value is `yes`, `true` or `1`; will not prompt otherwise. Default value is `yes` for reviewing the licenses.
-   **`ASDK_ACCEPT_LICENSES`:** Automatically accept prompted licenses on entrypoint if value is `yes`, `true` or `1`, will not automatically accept them otherwise. This value is meaningless if there is no prompt to accept licenses (customized by `ASDK_PROMPT_LICENSES` environment variable). The value is unset by default for you to manually review the licenses. _Note: Please read and review the licenses, and only accept them if you agree to them. Only **automatically** accept them in non-interactive (automated, like CI) environments after reviewing and being very familiar with the licenses._
-   **`ASDK_ACCEPT_LICENSES_SILENT`:** If automatically accepting licenses, it will not print the licenses on the STDOUT if set to `yes`, `true` or `1`. This is good to keep logs clean in non-interactive environments. Unset by default
-   **`ASDK_EXTRA_LOCALES`:** (not available in `alpine`) Generate new locales (`en_US.UTF-8` is already generated) on entrypoint. Accepts any value supported by [`locale` and `locale-gen`](https://man7.org/linux/man-pages/man1/locale.1.html). Multiple locales can be set by separating them with a comma (`,`). New locales will not be generated if this is unset.

Read more how to use them in the next sections

### Automatically Accepting Licenses
_First, remember to be very familiar with Android SDK licenses before automatically accepting it._
To automatically accept them, use the `ASDK_ACCEPT_LICENSES` environment variable to `yes`, `true` or `1`.
```bash
$ docker run --rm -it --name android-sdk --env "ASDK_ACCEPT_LICENSES=yes" redemonbr/android-sdk:alpine-latest /bin/sh
```

For use in automated environments, such like a CI, combine with `ASDK_ACCEPT_LICENSES_SILENT` to avoid having licenses being shown on logs
```bash
$ docker run --rm -it --name android-13 --env "ASDK_ACCEPT_LICENSES=yes" --env "ASDK_ACCEPT_LICENSES_SILENT=yes" redemonbr/android-sdk:ubuntu-api-33 /bin/bash
```

On CI, like GitLab-CI, this would be something like this:
```yml
build:android:
  stage: build
  image: redemonbr/android-sdk:alpine-stable
  variables:
    ASDK_ACCEPT_LICENSES: "yes"
    ASDK_ACCEPT_LICENSES_SILENT: "yes"
  script:
    - sh build-app.sh
```

### Locales Variables
_THIS IS NOT SUPPORTED IN ALPINE OR SCRATCH IMAGE VARIANTS_
The images already contain the locale `en_US.UTF-8` generated and set in the environment variables `LANG` and `LC_ALL` (also `LANGUAGE=en_US:en`).
If you need another `LANG` that is not already generated, you can set it like below:
-   **`ASDK_EXTRA_LOCALES`:** generate new locales on entrypoint
-   **`LANG`:** change `LANG` shell variable
```bash
$ docker run --rm -it --name node-android-11 --env "ASDK_EXTRA_LOCALES=en_GB.UTF-8,de_AT.UTF-8" --env "LANG=de_AT.UTF-8" redemonbr/android-sdk:node-18-api-32 /bin/bash
Generating locales (this might take a while)...
  de_AT.UTF-8... done
  en_GB.UTF-8... done
  en_US.UTF-8... done
Generation complete.
root@a410113043f0:/# _
```
Note that `en_US.UTF-8` still being generated by default.

Also, if you need to add more languages, `ASDK_EXTRA_LOCALES` can receive multiple languages in CSV format, like this:
```bash
$ docker run --rm -it --name buster --env "ASDK_EXTRA_LOCALES=en_GB.UTF-8,en_US.UTF-8,pt_BR,pt_BR.UTF-8" --env "LANG=en_GB.UTF-8,pt_BR" redemonbr/android-sdk:buster /bin/bash
Generating locales (this might take a while)...
  en_GB.UTF-8... done
  en_US.UTF-8... done
  pt_BR.ISO-8859-1... done
  pt_BR.UTF-8... done
Generation complete.
root@a410113043f0:/# _
```

If you don't want this behavior, just leave the `ASDK_EXTRA_LOCALES` environment variable unset.

## Using sdkmanager CLI
If you need to install more Android Packages, you can use `sdkmanager` CLI.
For example, if you need to install `Android Build Tools` on a specific version (`30.0.3` in this example):
```bash
## To automatically accept the prompt to download and install the package (if you already have read and reviewed it - and agree to it)
## You can echo 'y' to the sdkmanager command
## This is useful in non-interactive environments
$ echo y | sdkmanager --install "build-tools;30.0.3"
```
```bash
## In interactive environments you don't need to echo 'y' so you can manually review the licenses
$ sdkmanager --install "build-tools;30.0.3"
```


## Continuous Integration example
### GitLab-CI
Using on GitLab-CI with an [Ionic V3](#https://ionicframework.com), [Cordova](https://cordova.apache.org) and Angular v5.
Consider all build dependencies (Angular, Ionic and Cordova) declared in `package.json`'s `devDependencies`, so `npm install` should install them without the need to explicitly install them globally.
Also, their specific commands can be prefixed with `npx`. [Read more about npx here](https://docs.npmjs.com/cli/v7/commands/npx)
```yml
build:android:
  stage: build
  image: redemonbr/android-sdk:node-18-api-31
  variables:
    # this will automatically accept the licenses and not print them in the STDOUT for cleaner logs
    ASDK_ACCEPT_LICENSES: yes
    ASDK_ACCEPT_LICENSES_SILENT: yes
    # this will generate a new locale on entrypoint
    ASDK_EXTRA_LOCALES: en_GB.UTF-8
  script:
    - npm install
    - npx ionic cordova build android --prod
  after_script:
    # copying generated build for saving it as an artifact
    - cp platforms/android/app/build/outputs/apk/debug/app-debug.apk my-app-debug.apk
  artifacts:
    paths:
      - my-app-debug.apk
    expire_in: 1 month
    public: false
```


## Note for Apache Cordova users on NodeJS image variants
Since `cordova-android>=10.0.0` Java/JDK 11 is supported. NodeJS image variants including Java/JDK 8 are not built anymore.
If you are still using `cordova-android<10.0.0`, please update to a newer version that supports Java/JDK 11.
Read the release post from Apache Cordova about this [here](https://cordova.apache.org/announcements/2021/07/20/cordova-android-10.0.0.html)


## Note for deprecated images
These images have no support from upstream anymore and will not be built anymore. Note that, if you are still using one of them, please update to newer releases
-   `alpine3.15`: Images based on Alpine 3.15 will not be built anymore after 2023-11-01 because upstream general support ends at 2023-11-01. Recommend moving to `alpine3.16` or newer
-   `node-16`: Images based on NodeJS v16 will not be built anymore after 2023-09-11 because upstream general support for NodeJS v16 ends at 2023-09-11. Recommend moving to `node-18` or newer
-   `node-16-alpine`: Images based on NodeJS v16 Alpine will not be built anymore after 2023-09-11 because upstream general support for NodeJS v16 ends at 2023-09-11. Recommend moving to `node-18-alpine` or newer

## License
For all distributed images based on `buildpack-deps` images, view [buildpack-deps in Docker Hub](https://hub.docker.com/_/buildpack-deps) and [Debian Free Software Guidelines](https://www.debian.org/social_contract#guidelines).

For all distributed images based on `alpine` images, view [Alpine in Docker Hub](https://hub.docker.com/_/alpine)

All source code in this repository follows MIT License.
View [MIT License](https://gitlab.com/rdnxk/android-sdk-docker/-/blob/master/LICENSE.md) in the source code repository


## FAQ
-   More Ubuntu- or Debian-based variants planned?
    -   No. Only active LTS and currently maintained versions will be supported
-   Any CentOS or RedHat image variants planned?
    -   No. If you want glib and friends, checkout the Ubuntu- or Debian-based variants
-   More NodeJS images planned?
    -   No. Only NodeJS active LTS and currently maintained versions will be supported. The base OS on them are the latest Debian and Alpine versions (Bookworm and 3.18, respectivelly, at the time of this writing) will be supported
-   Any plans for using slim images from Debian?
    -   Nope. Heavy toolings are required for building Android apps, like JDK, Gradle and Android-SDKs. Using the slimmer images made no notable effect in the end size during my initial tests. I recommend checking out the Alpine variants for smaller pulls
