## PREDEFINED CONSTANTS

### BUILD CONFIG
CMDLINE_TOOLS_VERSION :::= 9477386
ANDROID_API_VERSIONS :::= 34,33,32,31,30
ANDROID_BUILD_TOOLS_VERSION :::= 34.0.0
ALIAS_SUFFIX_DEFAULT :::= api-33
ALIAS_SUFFIX_LATEST :::= api-33
ALIAS_SUFFIX_STABLE :::= api-31
ALIAS_SUFFIX_NEXT :::= api-34
ALIAS_PREFIX_DEFAULT :::= jammy

### MAKE CONFIG
TEMPLATES_DIR :::= ./templates
TEMP_DIR :::= .tmp

### UTILS
comma :::= ,
CONFIG_TYPE :::= application/vnd.rdnxk.manifest.config.v1+json
PEM_TYPE :::= application/x-pem-file
MARKDOWN_TYPE :::= text/markdown
PUBLICKEY_DESCRIPTION :::= PEM-formatted public key for validating images and artifacts built by rdnxk and ReDemoNBR
LICENSE_DESCRIPTION :::= License for this image
README_DESCRIPTION :::= Usage of this image
